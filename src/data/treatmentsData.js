/* src > data > treatmentsData.js */

const treatmentsData = [
    {
        id: "CPT 97035",
        name: "Light Therapy/Phototherapy",
        description: "Promotes tissue repair and natural healing in and around bones, joints, muscles, ligaments and tendons..",
        price: 200,
        onOffer: true
    },
    {
        id: "CPT 97024",
        name: "Diathermy",
        description: "Uses a high-frequency electric current to stimulate heat generation within body tissues.",
        price: 155,
        onOffer: true
    },
    {
        id: "CPT 97140",
        name: "Manual therapy",
        description: "The skilled application of passive movement to a joint either within or beyond its active range of movement.",
        price: 76,
        onOffer: true
    }
]

export default treatmentsData;