/* src > pages > TreatmentView.js */

import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

    //export default function CourseView() {
	export default function TreatmentView() {

	// The "useParams" hook allows us to retrieve any parameter or the courseId passed via the URL
	//const { courseId } = useParams();
	const { treatmentId } = useParams();
	const { user } = useContext(UserContext);
	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const history = useHistory();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll = (treatmentId) => {

		    //fetch(`http://localhost:4000/api/users/enroll`, {
		    fetch(`http://localhost:4000/api/orders/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				//courseId: courseId
				treatmentId: treatmentId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Successfully booked",
					icon: 'success',
					text: "You have successfully booked this treatment."
				});

				// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" component
				
				//history.push("/activetreatments");
				history.push("/treatments");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		});

	};

	useEffect(()=> {

		console.log(treatmentId);

		fetch(`http://localhost:4000/api/treatments/${treatmentId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [treatmentId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>USD {price}</Card.Text>
							
							{/*<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>*/}

							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => enroll(treatmentId)}>Book</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/veruser">Log in to Book a Treatment</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}