/* src > pages > Error.js */

import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "Whoopsie Daisy!",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}