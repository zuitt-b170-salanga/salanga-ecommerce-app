/* src > pages > Home.js */

import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
	    title: "Therapy & Rehab Solutions",
	    content: "Getting back on your feet is our priority.",
	    destination: "/activetreatments",
	    label: "Request Appointment"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}