/* src > components > UserView.js */

import { Fragment, useState, useEffect } from 'react'
import TreatmentCard from "./TreatmentCard";

// export default function UserView({coursesData}) {
export default function UserView({treatmentsData}) {

    // console.log(coursesData);

    // const [courses, setCourses] = useState([]);
    const [treatments, setTreatments] = useState([]);

    useEffect(() => {
        
        // Map through the courses received from the parent component (courses page) to render the course cards
        // const coursesArr = coursesData.map(course => {
        const treatmentsArr = treatmentsData.map(treatment => {
            // Returns active courses as "CourseCard" components
        	// if(course.isActive === true){
        	if(treatment.isActive === true){
				return (
					// <CourseCard courseProp={course} key={course._id}/>
					<TreatmentCard treatmentProp={treatment} key={treatment._id}/>
				)
        	}else{
        		return null;
        	}
        });

        // Set the "courses" state with the course card components returned by the map method
        // Allows the course card components to be rendered in this "UserView" component via the return statement below
        // setCourses(coursesArr);
        setTreatments(treatmentsArr);

    // }, [coursesData]);
     }, [treatmentsData]);

    return(
        <Fragment>
            {treatments}
        </Fragment>
    );
}
