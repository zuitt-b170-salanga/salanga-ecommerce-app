/* src > components > Highlights.js */

import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
    return (
        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Who We Treat</h2>
                        </Card.Title>
                        <Card.Text>
                            The patient population ranges from infants with developmental disabilities to the geriatric population and encompasses patients who have functional deficits from accidents or acute medical events and those deficits from progressive disease or chronic disorders.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Rehabilitation</h2>
                        </Card.Title>
                        <Card.Text>
                            The intensity of rehabilitation services provides varieties according to the patient’s severity of illness.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Treatments</h2>
                        </Card.Title>
                        <Card.Text>
                            Regardless of the setting, therapy is not considered justified after functional plateaus have been reached.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}

