/* src > components > AppNavbar.js */

import { Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

    const { user } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand as={Link} to="/">Therapy & Rehab Solutions</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">
                    <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
                    <Nav.Link as={NavLink} to="/treatments" exact>Treatments</Nav.Link>
                    {(user.id !== null) ? 
                            <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
                        : 
                            <Fragment>
                                <Nav.Link as={NavLink} to="/veruser" exact>Login</Nav.Link>
                                <Nav.Link as={NavLink} to="/reguser" exact>Register</Nav.Link>
                            </Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
// ===========================================
